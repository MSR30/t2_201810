package model.vo;

import model.data_structures.LinkedList;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{

	private String taxiId;
	
	private String company;
	
	private LinkedList<Service> services;
	
	public Taxi(String pId, String pCompany)
	{
		taxiId = pId;
		company =pCompany;
		services = new LinkedList<>();
	}
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxiId;
	}

	/**
	 * @return company
	 */
	public String getCompany() {
		// TODO Auto-generated method stub
		return company;
	}
	
	public void addService(Service service)
	{
		services.add(service);
	}
	
	public LinkedList<Service> getListServices()
	{
		return services;
	}
	
	@Override
	public int compareTo(Taxi o) {
		
		if(taxiId.compareTo(o.getTaxiId())<0)
		{
			return -1;
		}
		else if(taxiId.compareTo(o.getTaxiId())>0)
		{
			return 1;
		}
		return 0;
	}	
}
