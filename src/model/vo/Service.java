package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {

	private String tripId;
	
	private String taxiId;
	
	private double tripSeconds;
	
	private double tripMiles;
	
	private double tripTotal;
	
	private int area;
	
	public Service(String pTripId, String pTaxiId, double pTripSeconds, double pTripMiles, double pTripTotal, int pArea)
	{
		tripId = pTripId;
		taxiId = pTaxiId;
		tripSeconds = pTripSeconds;
		tripMiles = pTripMiles;
		tripTotal = pTripTotal;
		area = pArea;
	}
	
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		// TODO Auto-generated method stub
		return tripId;
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxiId;
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return (int) tripSeconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return tripMiles;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return tripTotal;
	}
	
	public int getArea()
	{
		return area;
	}

	@Override
	public int compareTo(Service o) {

		if(tripId.compareTo(o.getTaxiId())<0)
		{
			return -1;
		}
		else if(tripId.compareTo(o.getTaxiId())>0)
		{
			return 1;
		}
		
		return 0;
	}
}
