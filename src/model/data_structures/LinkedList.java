package model.data_structures;

public class LinkedList <T extends Comparable<T>> implements ILinkedList<T>{

	private Node<T> first;

	private Node<T> last;

	private Node<T> actual;


	public LinkedList()
	{
		first = null;
		last = null;
		actual = first;

	}

	public Node<T> getFirst()
	{
		return first;
	}

	public Node<T> getLast()
	{
		return last;
	}


	@Override
	public boolean add(T elem) {

		if(first==null)
		{
			first = new Node<T>(elem,null, null);
			last = first;
			actual = first;
			return true;
		}
		else
		{
			Node<T> x = new Node<T>(elem, null,last);
			last.changeNext(x);
			last = x;
			return true;
		}

	}

	@Override
	public boolean delete(T elem) {

		if(first!=null)
		{
			if(first.getElem().compareTo(elem)==0)
			{
				first.getNext().changePrev(null);
				first = first.getNext();
				return true;
			}
			else
			{
				Node<T> act = first;
				while(act.getNext()!=null)
				{
					if(act.getNext().equals(last) && act.getNext().getElem().equals(elem))
					{
						last = act;
					}

					if(act.getNext().getElem().equals(elem))
					{
						act.changeNext(act.getNext().getNext());
						if(act.getNext()!=null)
							act.getNext().changePrev(act);

						return true;
					}
					act = act.getNext();
				}
			}
		}
		return false;
	}

	@Override
	public T get(T elem) {

		if(first!=null){
			if(first.getElem().compareTo(elem)==0)
			{
				return (T) first.getElem();
			}
			else
			{
				Node<T> act = first;
				while(act.getNext()!=null)
				{
					act = act.getNext();
					if(act.getElem().compareTo(elem)==0)
					{
						return (T) act.getElem();
					}

				}
			}
		}
		return null;
	}

	@Override
	public int getSize() {

		Node<T> act = first;
		int size = 0;
		while(act!=null)
		{
			size+=1;
			act = act.getNext();
		}
		return size;
	}

	@Override
	public T getByPosition(int pos) {

		if(pos<=getSize())
		{
			int posAct = 0;
			Node<T> act = first;
			if(pos==0)
			{
				return (T) act.getElem();
			}
			else{
				while(posAct!=pos)
				{
					posAct = posAct+1;
					act = act.getNext();
					if(posAct==pos)
					{
						return (T) act.getElem();
					}
				}
			}
		}
		return null;
	}

	@Override
	public void listing() 
	{
		actual = first;
	}

	@Override
	public T getCurrent() {
		
		return (T) actual.getElem();
	}

	@Override
	public T next() {

		if(actual.getNext()!=null)
		{
			actual = actual.getNext();
			return (T) actual.getElem();
		}
		actual = null;
		return (T) actual;
	}

}
