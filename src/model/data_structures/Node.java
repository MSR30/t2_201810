package model.data_structures;

public class Node <T extends Comparable<T>>
{

	private T elem;
	
	private Node<T> next;
	
	private Node<T> prev;
	
	public Node(T pElem,Node<T> pNodeN, Node<T> pNodeP)
	{
		elem = pElem;
		next = pNodeN;
		prev = pNodeP;
	}
	
	public void changeNext (Node<T> elem)
	{
		next = elem;
	}
	
	public void changePrev (Node<T> elem)
	{
		prev = elem;
	}
	
	public Node<T> getNext()
	{
		return next;
	}
	
	public Node<T> getPrev()
	{
		return prev;
	}
	
	public T getElem()
	{
		return elem;
	}
}
