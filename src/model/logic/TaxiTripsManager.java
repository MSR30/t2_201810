package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import api.ITaxiTripsManager;
import model.vo.Taxi;
import model.vo.Service;
import model.data_structures.LinkedList;

public class TaxiTripsManager implements ITaxiTripsManager {

	// TODO
	// Definition of data model 
	private LinkedList<Taxi> listTaxis;

	public void loadServices (String serviceFile) {
		JsonParser parser = new JsonParser();

		listTaxis = new LinkedList<>();

		try {
			String taxiTripsDatos = serviceFile;

			/* Cargar todos los JsonObject (servicio) definidos en un JsonArray en el archivo */
			JsonArray arr= (JsonArray) parser.parse(new FileReader(taxiTripsDatos));

			/* Tratar cada JsonObject (Servicio taxi) del JsonArray */
			for (int i = 0; arr != null && i < arr.size(); i++)
			{
				JsonObject obj=  (JsonObject) arr.get(i);

				String company = "NaN";
				if ( obj.get("company") != null )
				{ company = obj.get("company").getAsString();}

				int pickup_community_area = 0;
				if ( obj.get("pickup_community_area") != null )
				{ pickup_community_area = obj.get("pickup_community_area").getAsInt(); }

				String taxi_id = "NaN";
				if ( obj.get("taxi_id") != null )
				{ taxi_id = obj.get("taxi_id").getAsString(); }

				String  trip_id = "NaN";
				if ( obj.get("trip_id") != null )
				{  trip_id = obj.get("trip_id").getAsString(); }

				double trip_miles = 0;
				if ( obj.get("trip_miles") != null )
				{ trip_miles = obj.get("trip_miles").getAsDouble(); }

				double trip_seconds = 0;
				if ( obj.get("trip_seconds") != null )
				{ trip_seconds = obj.get("trip_seconds").getAsDouble(); }

				double trip_total = 0;
				if ( obj.get("trip_total") != null )
				{ trip_total = obj.get("trip_total").getAsDouble(); }	

				Taxi taxi = new Taxi(taxi_id, company);
				Service service = new Service(trip_id, taxi_id, trip_seconds, trip_miles, trip_total, pickup_community_area);
				if(getTaxi(taxi)==null)
				{
					listTaxis.add(taxi);
					taxi.getListServices().add(service);
				}
				else
				{
					Taxi taxi2 = getTaxi(taxi);
					taxi2.getListServices().add(service);
				}

			}	
		}
		catch (JsonIOException e1 ) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		catch (FileNotFoundException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
	}

	@Override
	public LinkedList<Taxi> getTaxisOfCompany(String company) {

		LinkedList<Taxi> listT = new LinkedList<>();

		for (int i = 0; i < listTaxis.getSize(); i++) {

			Taxi taxi = listTaxis.getByPosition(i);

			if(taxi.getCompany().equals(company))
			{
				listT.add(taxi);
			}
		}

		System.out.println("Inside getTaxisOfCompany with " + company);
		return listT;
	}

	@Override
	public LinkedList<Service> getTaxiServicesToCommunityArea(int communityArea) {

		LinkedList<Service> listS = new LinkedList<>();

		for (int i = 0; i < listTaxis.getSize(); i++) {

			Taxi taxi = listTaxis.getByPosition(i);

			for (int j = 0; j < taxi.getListServices().getSize(); j++) {

				int area = taxi.getListServices().getByPosition(j).getArea();

				if(area == communityArea)
				{
					listS.add(taxi.getListServices().getByPosition(j));

				}
			}


		}
		System.out.println("Inside getTaxiServicesToCommunityArea with " + communityArea);
		return listS;
	}

	public Taxi getTaxi(Taxi elem)
	{
		return listTaxis.get(elem);
	}


}
