package uniandes.LinkedList.test;


import junit.framework.TestCase;
import model.data_structures.LinkedList;
import model.data_structures.Node;


public class LinkedListTest extends TestCase{

	private LinkedList<String> list;
	
	private LinkedList<String> list2;

	
	public void setupEscenario1( )
	{
		list = new LinkedList<String>( );
		list.add("hola");
		list.add("yes");
		list.add("not");
		
	}
	
	public void setupEscenario2( )
	{
		list2 = new LinkedList<String>( );
		
	}

	// -----------------------------------------------------------------
	// M�todos de prueba
	// -----------------------------------------------------------------


	public void testAgregarObjeto()
	{
		setupEscenario1();
		boolean result = list.add("maybe");
		boolean result2 = list.add("one");
		boolean result3 = list.add("two");
		
		assertTrue("Deberia haberse a�adido", result);
		assertTrue("Deberia haberse a�adido", result2);
		assertTrue("Deberia haberse a�adido", result3);
		
		assertNull("Deberia ser null", list.getFirst().getPrev());
		assertNull("Deberia ser null", list.getLast().getNext());
		
		assertEquals("Deberian ser el mismo", "one", list.getLast().getPrev().getElem());

	}
	
	public void testEliminarObjeto()
	{
		setupEscenario2();
		list2.add("hola");
		list2.add("yes");
		list2.add("not");
		
		boolean result = list2.delete("hola");
		
		assertTrue("Deberia haberse eliminado", result);
		assertEquals("No son el mismo", "yes", list2.getByPosition(0));
		
		assertNull("Deberia ser null", list2.getFirst().getPrev());
		assertNull("Deberia ser null", list2.getLast().getNext());
		
	}
	
	public void testGetElem()
	{
		setupEscenario1();
		
		assertEquals("Deberian ser el mismo", "hola", list.get("hola"));
		
	}
	
	public void testGetSize()
	{
		setupEscenario1();

		assertEquals("Deberian ser el mismo", 3, list.getSize());
		
	}
	
	public void testGetByPosition()
	{
		setupEscenario1();

		assertEquals("Deberian ser el mismo", "hola", list.getByPosition(0));
		assertEquals("Deberian ser el mismo", "yes", list.getByPosition(1));
		assertEquals("Deberian ser el mismo", "not", list.getByPosition(2));
		
	}
	
	public void testListing()
	{
		setupEscenario1();
		String a = list.next();
		
		assertNotSame("Son el mismo", "hola", a);
		list.listing();
		
		assertEquals("Deberian ser el mismo", "hola", list.getCurrent());
	}
	
	public void testNext()
	{
		setupEscenario1();
		
		assertEquals("Deberian ser el mismo", "hola", list.getCurrent());
		list.next();
		assertEquals("Deberian ser el mismo", "yes", list.getCurrent());

	}
	
	
}
